import React, { Component } from 'react'
import './App.css';
import ReactDOM from 'react-dom';
import GoogleMapReact from 'google-map-react'
import axios from 'axios';
import Marker from './Marker';
import {withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer} from 'react-google-maps'

class SimpleMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      from: '',
      to: '',
      center: {
        lat: 0,
        lng: 0
      },
      zoom: 11,
      map: {},
      maps: {},
      origin: {
        lat: 0,
        lng: 0,
      },
      destination: {
        lat: 28.72082,
        lng: 77.107241,
      }
    };
    this.onClickHandler = this.onClickHandler.bind(this);
    this.apiIsLoaded = this.apiIsLoaded.bind(this);
  }

  handleToChange = (e) => {
    this.setState({
      to: e.target.value
    });
  }
  handleFromChange = (e) => {
    this.setState({
      from: e.target.value
    });
  }

  apiIsLoaded = (map, maps) => {
    if (maps) {
      const directionsService = new maps.DirectionsService();
      const directionsDisplay = new maps.DirectionsRenderer();
      this.setState({
        map, maps,
      }, () => {
        this.setState({
          directionsService,
          directionsDisplay
        })
      })
    }
  };

  render() {
    return (
      <div className="container">
        <div className="map" style={{ height: '100vh', width: '80%' }}>
          <GoogleMapReact
            bootstrapURLKeys={{ key: 'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94' }}
            defaultCenter={this.state.center}
            defaultZoom={this.state.zoom}
            center={this.state.center}
            yesIWantToUseGoogleMapApiInternals={true}
            onGoogleApiLoaded={({ map, maps }) => this.apiIsLoaded(map, maps)}
          >
          
            <Marker lat={this.state.origin.lat} lng={this.state.origin.lng} />
            <Marker lat={this.state.destination.lat} lng={this.state.destination.lng} />
          </GoogleMapReact>
        </div>
        <div className="right-side">
          <h1> WITAJ ! </h1>
          <p> co chcesz wyszukac ?</p>
          <input id="btn" value={this.state.from} onChange={this.handleFromChange} />
          <input id="btn3" value={this.state.to} onChange={this.handleToChange} />
          <input className="btn2" type="submit" value="Kliknij" onClick={this.onClickHandler} />
        </div>
      </div>
    );
  }

  onClickHandler() {
    var start = document.getElementById("btn").value;
    var end = document.getElementById("btn3").value;
    // var link = `https://maps.googleapis.com/maps/api/directions/json?origin=${txt}&destination=${txt3}`
    //  console.log(link)

    axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
      params: {
        address: start,
        key: 'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94'
      }
    })
      .then((response) => {
        console.log(response);
        this.setState({ origin: { lat: response.data.results[0].geometry.location.lat, lng: response.data.results[0].geometry.location.lng } })
      })

      .catch(function (error) {
        console.log(error);
      })
      axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
        params: {
          address: end,
          key: 'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94'
        }
      })
        .then((response) => {
          console.log(response);
          this.setState({ destination:  { lat: response.data.results[0].geometry.location.lat, lng: response.data.results[0].geometry.location.lng } })
        })
  
        .catch(function (error) {
          console.log(error);
        })
  
        // var start = new GoogleMap.maps.LatLng(this.state.origin.lat,this.state.origin.lng);
        // var end = new GoogleMap.maps.LatLng(this.state.destination.lat,this.state.destination.lng)

    const maps = this.state.maps;
    const directionsService = this.state.directionsService;
    const directionsDisplay = this.state.directionsDisplay;
    var request = {
      origin: start,
      destination: end,
      travelMode: 'DRIVING'
    }

    directionsService.route(request, (result, status) => {

      if (status == 'OK') {
        directionsDisplay.setDirections(result);
        // this.setState({
        //   origin: { lat: response.routes[0].legs[0].start_location.lat, lng: response.routes[0].legs[0].start_location.lng },
        //   destination: { lat: response.routes[0].legs[0].start_location.lat, lng: response.routes[0].legs[0].start_location.lng }
        // })
        //  console.log(response)
        console.log(result);

        this.setState(
          { directions: result });

      }
      else {
        window.alert('Directions request failed due to ' + status);
      }
    });

  }
}
ReactDOM.render(<SimpleMap />, document.getElementById("root"));

/*global google*/
import React, { Component, StyleSheet } from 'react'
import './App.css';
import ReactDOM from 'react-dom';
import axios from 'axios';

import { GoogleMap, Marker, withGoogleMap, withScriptjs, DirectionsRenderer} from 'react-google-maps';
import MarkerClusterer from "react-google-maps/lib/components/addons/MarkerClusterer";
import { lifecycle } from 'recompose';

const GoogleMaps = withScriptjs(withGoogleMap(props => {
  const { onMapMounted, ...otherProps } = props;
  return <GoogleMap {...otherProps} ref={c => {
    onMapMounted && onMapMounted(c)
  }}>{props.children}</GoogleMap>
}));

class SimpleMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      isMarkerShown: false,
      isLocated: true,
      isRoute: false,
      from: '',
      to: '',
      find: '',
      center: {
        lat: 0,
        lng: 0
      },
      distance: '',
      duration: '',
    };
    this.onClickCalc= this.onClickCalc.bind(this)
  }




  // AKTUALNA POZYCJA 

  showCurrentLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.setState(prevState => ({
          center: {
            ...prevState.center,
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          },
          isMarkerShown: true,
          isLocated: true,

        }))
      }
      )
    }
    else {
      error => console.log(error)
    }
  }
  componentDidMount() {
    this.showCurrentLocation()
  }
  // <--- ODCZYTUJE DANE PUNKTU ---> 

  onClickFind = (e) => {
    e.preventDefault();
    let adres = document.getElementById("find").value;
    axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
      params: {
        address: adres,
        key: 'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94'
      }
    })
      .then((response) => {

        let lat = response.data.results[0].geometry.location.lat;
        let lng = response.data.results[0].geometry.location.lng;
        var el = document.getElementById("info");
        let html = '';
        const div = document.querySelector(".right-side");
        document.getElementById('from').value = "";
        document.getElementById('to').value = "";
        div.appendChild(el).innerHTML = html;

        this.setState({
          center: {
            lat: response.data.results[0].geometry.location.lat,
            lng: response.data.results[0].geometry.location.lng,

          },
          origin: {
            lat: null,
            lng: null
          },
          isMarkerShown: true,
          isLocated: true,
          isRoute: false,
          zoom: 16,
        },
        )
      })

      .catch(function (error) {
        console.log(error);
      })
  }


// <--- ODCZYTUJE DROGE ---> 

onClickFindRoute = (e) => {
  e.preventDefault();
  var selectMode = document.getElementById('mode').value;
  const DirectionsService = new google.maps.DirectionsService();
  DirectionsService.route({
    origin: document.getElementById('from').value,
    destination: document.getElementById('to').value,
    travelMode: google.maps.TravelMode[selectMode],
    provideRouteAlternatives: true,
    optimizeWaypoints: true
  }, (result, status) => {
    if (status === google.maps.DirectionsStatus.OK) {
      console.log(result)
      let distance = result.routes[0].legs[0].distance.text;
      let duration = result.routes[0].legs[0].duration.text;
      this.setState
        ({
          distance: distance,
          duration: duration,
          directions: result,
          isLocated: false,
          isRoute: true,

        })

      document.getElementById('find').value = "";
      const el = document.getElementById("info");
      if(selectMode==="DRIVING"){
       
        let html = "<div>"
         html += '<form >';
        //  html += '<select id="modeType">' ;
        // html += '<option value=""> SELECT OPTION </option>';
        // html += '<option value="CAR">CAR</option>';
        // html += '<option value="BUS">BUS</option>';
        // html += '<option value="TRUCK">TRUCK</option>';
        // html+= '</select>';
        html += '<input id="fuel" placeholder="Fuel Consuption" />' ;
        html += '<input id="cost" placeholder="Cost Fuel" />' ;
        html += '<input id="Calc" type="submit" value="Kliknij" />';
        html+= ' </form>'
        html+= '</div>';
        html += '<h4>Ilosc KM : </h4>' + distance;
        html += ' <h4>Czas przejazdu : </h4> ' + duration;
        var dst = distance;
        const div = document.querySelector(".right-side");
        div.appendChild(el).innerHTML = html;
        let btn = document.getElementById('Calc');
        let fuel = document.getElementById('fuel');
        let cost = document.getElementById('cost');

        btn.addEventListener("click", function onClickAdd(event){
          var fuelValue = parseFloat(fuel.value);
          var costValue = parseFloat(cost.value);
          var DistanceValue = parseInt(dst);
      
          if(typeof fuelValue === "number" && typeof costValue === "number"){
            const resultTrack =  (DistanceValue * fuelValue)/100;
            const resultCost = resultTrack * costValue;
            
            html += '<h4>Ilosc paliwa  : </h4>' + Math.round(resultTrack*100)/100;
      
            html += ' <h4>Koszt przejazdu : </h4> ' + Math.round(resultCost*100)/100;;     
            div.appendChild(el).innerHTML = html;
          }
          else{
            alert("Wprowadz liczbe ");
          }
        });
      }
      else{
        let html= '<h4>Ilosc KM : </h4>' + distance;
        html += ' <h4>Czas przejazdu : </h4> ' + duration;
        const div = document.querySelector(".right-side");
        div.appendChild(el).innerHTML = html;
      }
      document.getElementById('from').value = "";
      document.getElementById('to').value = "";

    }

  },
  )

}
onClickCalc= (e) =>{
        
          let consuption = document.getElementById('fuel').value;
          alert(consuption);
          alert(typeof(consuption));
           if(typeof(consuption)=== 'number'){
             console.log('dziala')
           }
            else{
             alert('wpisz liczbe')
           }
}
render() {
  return (

    <div className="container">
      <div className="map">
        <GoogleMaps
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDW3Cj0IaHpHXkMfOFItT_83Ai5lI2X34U"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `900px`, width: `100%` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          defaultZoom={16}
          center={this.state.center}
        >

          {this.state.isRoute && this.state.isMarkerShown && <DirectionsRenderer directions={this.state.directions} />}

          {this.state.isLocated && this.state.isMarkerShown && <Marker position={{ lat: this.state.center.lat, lng: this.state.center.lng }} />}
        </GoogleMaps>
      </div>
      <div className="right-side">
        <h1> WITAJ! </h1>
        <p> co chcesz wyszukac ?</p>

        <form onSubmit={this.onClickFind}>
          <input id="find" placeholder="SEARCH" />
          <input id="findbutton" type="submit" value="Kliknij" />
        </form>
        <h1> ZNAJDZ TRASĘ </h1>
        <form onSubmit={this.onClickFindRoute}>
          <input id="from" placeholder="FROM" />
          <input id="to" placeholder="TO" />
          <select id="mode">
            <option value=""> SELECT OPTION </option>
            <option value="DRIVING">Driving</option>
            <option value="WALKING">Walking</option>
            <option value="BICYCLING">Bicycling</option>
            <option value="TRANSIT">Transit</option>
          </select>
          <input id="findroute" type="submit" value="Kliknij" />

        </form>

        <div id="info">
        </div>

      </div>

    </div>

  );
}

}


ReactDOM.render(<SimpleMap />, document.getElementById("root"));
export default withScriptjs(withGoogleMap(Map))

